#include <iostream>
#include <string>
#include "Headers/timeImp.h"

using namespace std;

TimeImp::TimeImp() {

}

TimeImp::TimeImp(int hr, int min) {
    hour = hr;
    minutes = min;
}

void TimeImp::tell() {
    cout << "Time is: " << hour << ":" << minutes << endl;
}