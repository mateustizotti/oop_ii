#include <iostream>
#include <string.h>
#include "Headers/civilianTimeImp.h"

using namespace std;

CivilianTimeImp::CivilianTimeImp() {

}

CivilianTimeImp::CivilianTimeImp(int hr, int min, int m) : TimeImp(hr, min) {
    if (m) {
        strcpy (whichM, " PM");
    } else  {
        strcpy (whichM, " AM");
    }
}

void CivilianTimeImp::tell() {
    cout << "Time is: " << hour << ":" << minutes << whichM << endl;
}
