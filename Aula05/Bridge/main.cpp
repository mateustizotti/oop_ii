#include <iostream>
#include <string>
#include "Headers/time.h"
#include "Headers/civilianTime.h"
#include "Headers/zuluTime.h"
#include "Headers/brazilianTime.h"

using namespace std;

int main() {
   Time *times[4];
   times[0] = new Time(14, 30);
   times[1] = new CivilianTime(2, 30, 1);
   times[2] = new ZuluTime(14, 30, 6);
   times[3] = new BrazilianTime(14, 30);

   for (int i = 0; i < 4; i++) {
       times[i]->tell();
   }
   
   return 0;
}