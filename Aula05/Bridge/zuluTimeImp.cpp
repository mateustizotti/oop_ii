#include <iostream>
#include <string.h>
#include "Headers/zuluTimeImp.h"

using namespace std;

ZuluTimeImp::ZuluTimeImp() {

}

ZuluTimeImp::ZuluTimeImp(int hr, int min, int z) : TimeImp(hr, min) {
    if (z == 5) {
        strcpy(zone, " Eastern Standard Time");
    } else if (z == 6) {
        strcpy(zone, " Central Standard Time");
    }
    
}

void ZuluTimeImp::tell() { 
    cout << "Time is: " << hour << ":" << minutes << zone << endl;
}

