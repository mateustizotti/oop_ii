#include <iostream>
#include <string>
#include "Headers/brazilianTimeImp.h"

using namespace std;

BrazilianTimeImp::BrazilianTimeImp() {

}

BrazilianTimeImp::BrazilianTimeImp(int hr, int min) : TimeImp(hr, min) {

}

void BrazilianTimeImp::tell() {
    cout << "Time is: " << hour << ":" << minutes << endl;
}