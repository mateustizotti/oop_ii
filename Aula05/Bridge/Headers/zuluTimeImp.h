#ifndef ZULUTIMEIMP_H
#define ZULUTIMEIMP_H
#include <iostream>
#include <string>
#include "timeImp.h"

using namespace std;

class ZuluTimeImp : public TimeImp {
    protected:
        char zone[30];
    public:
        ZuluTimeImp();
        ZuluTimeImp(int hr, int min, int zone);
        void tell();
};

#endif