#ifndef CIVILIANTIME_H
#define CIVILIANTIME_H
#include <iostream>
#include <string>
#include "time.h"

using namespace std;

class CivilianTime : public Time {
    public:
        CivilianTime(int hr, int min, int m); 
};

#endif