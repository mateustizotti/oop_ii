#ifndef ZULUTIME_H
#define ZULUTIME_H
#include <iostream>
#include <string>
#include "time.h"

using namespace std;

class ZuluTime : public Time{
    public:
        ZuluTime(int hr, int min, int z); 
};

#endif