#ifndef TIMEIMP_H
#define TIMEIMP_H
#include <iostream>
#include <string>

using namespace std;

class TimeImp {
    protected:
        int hour, minutes;
    public:
        TimeImp();
        TimeImp(int hr, int min);
        virtual void tell();
};

#endif