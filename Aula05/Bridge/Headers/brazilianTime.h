#ifndef BRAZILIANTIME_H
#define BRAZILIANTIME_H
#include <iostream>
#include <string>
#include "time.h"

using namespace std;

class BrazilianTime : public Time {
    public:
        BrazilianTime(int hr, int min);
};

#endif