#ifndef BRAZILIANTIMEIMP_H
#define BRAZILIANTIMEIMP_H
#include <iostream>
#include <string>
#include "timeImp.h"

using namespace std;

class BrazilianTimeImp : public TimeImp {
    protected:
        
    public:
        BrazilianTimeImp();
        BrazilianTimeImp(int hr, int min);
        void tell();
};

#endif