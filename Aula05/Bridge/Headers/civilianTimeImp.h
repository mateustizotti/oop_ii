#ifndef CIVILIANTIMEIMP_H
#define CIVILIANTIMEIMP_H
#include <iostream>
#include <string>
#include "timeImp.h"

using namespace std;

class CivilianTimeImp : public TimeImp{
    protected:
        char whichM[4];
    public:
        CivilianTimeImp();
        CivilianTimeImp(int hr, int min, int m);
        void tell();
};

#endif