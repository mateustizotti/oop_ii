#ifndef TIME_H
#define TIME_H
#include <iostream>
#include <string>
#include "timeImp.h"

using namespace std;

class Time {
    protected:
        TimeImp *imp;
    public:
        Time();
        Time(int hr, int min);
        virtual void tell();
};

#endif