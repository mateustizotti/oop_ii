# Sistema de Gerenciamento de Pedidos Subway

***

## Autores

* Mateus Tizotti
* Wagner da Silva Dias

***

## Descrição do Sistema

O sistema que está sendo desenvolvido pela dupla tem por objetivo o gerenciamento de um comércio de alimentos muito semelhante ao estilo de funcionamento da rede Subway. 
Ao acessar o sistema é possível escolher dois tipos de alimentos: salada ou sanduíche. Após a escolha do tipo de alimento o usuário irá montar o prato desejado adicionando, recheios e acompanhamentos como molho, tipo de carne, tipo de salada e queijo.

***

## Utilização de Design Patterns

Para o desenvolvimento do sistema foi utilizado 5 padrões de projeto, sendo:

* Singleton: Utilizado para guardar registros de Nota Fiscal;
* Facade: Utilizado para simplificar o processo de fazer sanduíche, salada e gerar o pedido;
* Decorator: Utilizado para montar os pratos;
* Factory: Utilizado para delegar à subclasses a responsabilidade de decidir qual prato criar (Salada ou Sanduíche);
* Iterator: Utilizado para melhor navegar pelos vetores criados pelo sistema.

***

## UML e Esquematização do Sistema

![UML](https://bitbucket.org/mateustizotti/objectorientedprogramming/raw/5b166c0877d9636ad5860c4f63a78d9bbd8cac4b/GB/UML/uml_mini.png)

