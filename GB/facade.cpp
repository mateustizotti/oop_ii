#include <iostream>
#include <string>
#include "Headers/facade.h"

using namespace std;

Facade::Facade() {
    guiFactory = new Factory;
}

void Facade::copyVector(Order *temp) {
    for (int i = 0; i < itens.size(); i++) {
        temp->addIten(itens[i]);
    }
}

void Facade::makeOrder() {
    int a;
    a = selectOrderType();
    if (a == 1) {
        temp = guiFactory->createOrder("Sandwich");
        itens.push_back({ "Sandwich ---------- ", 10.0 });
        makeSandwich();
    } else if (a == 2) {
        temp = guiFactory->createOrder("Salad");
        itens.push_back({ "Salad ------------- ", 7.0 });
        makeSalad();
    }
}

void Facade::makeSandwich() {
    int var, isSelecting = 1;
    string added;
    
    int bread = selectBread();
    if (bread == 1) {
        temp = new Italian(temp);
        itens.push_back({ "Italian bread ----- ", 4.0 });
    } else if (bread == 2) {
        temp = new Parmesan(temp);
        itens.push_back({ "Parmesan bread ---- ", 5.0 });
    }

    int topping = selectTopping();
    if (topping == 1) {
        temp = new Teriaki(temp);
        itens.push_back({ "Teriaki chicken --- ", 7.0 });
    } else if (topping == 2) {
        temp = new Bbq(temp);
        itens.push_back({ "Barbecue ---------- ", 6.0 });
    }
    
    int cheese = selectCheese();
    if (cheese == 1) {
        temp = new Cheddar(temp);
        itens.push_back({ "Cheddar ----------- ", 4.0 });
    } else if (cheese == 2) {
        temp = new Swiss(temp);
        itens.push_back({ "Swiss cheese ------ ", 4.0 });
    }

    while(isSelecting) {
        int salad = selectSalads();
        if (salad == 1) {
            temp = new Lettuce(temp);
            itens.push_back({ "Lettuce ----------- ", 3.0 });
        } else if (salad == 2) {
            temp = new Picles(temp);
            itens.push_back({ "Picles ------------ ", 2.0 });
        } else if (salad == 3) {
            temp = new Tomato(temp);
            itens.push_back({ "Tomatoes ---------- ", 4.0 });
        } else if (salad == 4) {
            isSelecting = 0;
        }
    }

    isSelecting = 1;
    while (isSelecting)
    {
        int sauce = selectSauces();
        if (sauce == 1) {
            temp = new Chipotle(temp);
            itens.push_back({ "Chipotle ---------- ", 3.0 });
        } else if (sauce == 2) {
            temp = new HoneyMustard(temp);
            itens.push_back({ "Honey Mustard ----- ", 3.0 });
        } else if (sauce == 3) {
            temp = new Mayo(temp);
            itens.push_back({ "Mayo -------------- ", 3.0 });
        } else if (sauce == 4) {
            isSelecting = 0;
        }
    }

    for (int i = 0; i < itens.size(); i++) {
        cout << itens[i].name <<  itens[i].price << endl;
    }
    
    copyVector(temp);
    Singleton *a = Singleton::getInstance();
    a->addOrder(*temp);

    clear();
    cout << "Your order is completed!\n" << endl;
    cout << "Order description" << endl;
    cout << "-------------------------" << endl;
    temp->listInfo();
    cout << "-------------------------" << endl;
    cout << "Total ------------- R$" << temp->getPrice() << endl;
    cout << "-------------------------" << endl;
    cout << "\nType 0 and enter to proceed" << endl;
    cin >> var;
}

void Facade::makeSalad() {
    int var, isSelecting = 1;
    
    int topping = selectTopping();
    if (topping == 1) {
        temp = new Teriaki(temp);
        itens.push_back({ "Teriaki chicken --- ", 7.0 });
    } else if (topping == 2) {
        temp = new Bbq(temp);
        itens.push_back({ "Barbecue ---------- ", 6.0 });
    }

    while(isSelecting) {
        int salad = selectSalads();
        if (salad == 1) {
            temp = new Lettuce(temp);
            itens.push_back({ "Lettuce ----------- ", 3.0 });
        } else if (salad == 2) {
            temp = new Picles(temp);
            itens.push_back({ "Picles ------------ ", 2.0 });
        } else if (salad == 3) {
            temp = new Tomato(temp);
            itens.push_back({ "Tomatoes ---------- ", 4.0 });
        } else if (salad == 4) {
            isSelecting = 0;
        }
    }

    isSelecting = 1;
    while(isSelecting) {
        int sauce = selectSauces();
        if (sauce == 1) {
            temp = new Chipotle(temp);
            itens.push_back({ "Chipotle ---------- ", 3.0 });
        } else if (sauce == 2) {
            temp = new HoneyMustard(temp);
            itens.push_back({ "Honey Mustard ----- ", 3.0 });
        } else if (sauce == 3) {
            temp = new Mayo(temp);
            itens.push_back({ "Mayo -------------- ", 3.0 });
        } else if (sauce == 4) {
            isSelecting = 0;
        }
    }

    copyVector(temp);
    Singleton *a = Singleton::getInstance();
    a->addOrder(*temp);

    clear();
    cout << "Your order is completed!\n" << endl;
    cout << "Order description" << endl;
    cout << "-------------------------" << endl;
    temp->listInfo();
    cout << "-------------------------" << endl;
    cout << "Total ------------- R$" << temp->getPrice() << endl;
    cout << "-------------------------" << endl;
    cout << "\nType 0 and enter to proceed" << endl;
    cin >> var;
}

