#include <iostream>
#include <string>
#include "Headers/ingredients.h"
#include "Headers/order.h"
#include "Headers/sandwich.h"
#include "Headers/salad.h"
#include "Headers/GUIFactory.h"
#include "Headers/Factory.h"
#include "Headers/utils.h"
#include "Headers/facade.h"
#include "Headers/ITEN.h"

using namespace std;

int main() {
   int x, var;
menu:
   x = menu();
   switch (x) {
      case 1: {
         Singleton *a = Singleton::getInstance();
         a->listOrders();
         cout << "\nType 0 and enter to return\n" << endl;
         cin >> var;
         goto menu;
      }
      case 2: {
         Facade fac;
         fac.makeOrder();
         goto menu;
      }
      case 3: {
         cout << "Exiting...\nByeBye!" << endl;
         return 0;
      }
      default: {
         cout << "Not an option! Type 0 and enter to try again" << endl;
         cin >> var;
         goto menu;
      }
         
   }

   return 0;
}