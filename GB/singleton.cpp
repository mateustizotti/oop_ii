#include <iostream>
#include <string>
#include "Headers/singleton.h"
#include "Headers/utils.h"

using namespace std;

Singleton::Singleton() {}

Singleton* Singleton::instance = 0;

Singleton* Singleton::getInstance() {
    if (instance == 0) {
        instance = new Singleton();
    }
    return instance;
}

void Singleton::addOrder(Order& o) {
    nfes.push_back(&o);
}

void Singleton::listOrders() {
    clear();
    int n = 1;
    if (nfes.empty()) {
        cout << "There are currently no orders created!" << endl;
    } else {
        for (i = nfes.begin(); i != nfes.end(); ++i , ++n){
            cout << "--- " << "Order " << n << " ---" << endl;
            (*i)->listInfo();
            cout << "Total ------------- R$" << (*i)->getPrice() << endl;
            cout << endl;
        }
    }
}