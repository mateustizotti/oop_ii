#ifndef UTILS_H
#define UTILS_H
#include <iostream>
#include <string.h>
#include <vector>
#include "order.h"

#include "Bread/intalian.h"
#include "Bread/parmesan.h"

#include "Cheese/cheddar.h"
#include "Cheese/swiss.h"

#include "Toppings/bbq.h"
#include "Toppings/teriaki.h"

#include "Salads/lettuce.h"
#include "Salads/picles.h"
#include "Salads/tomato.h"

#include "Sauces/chipotle.h"
#include "Sauces/honeyMustard.h"
#include "Sauces/mayo.h"

using namespace std;

inline void clear() {
    system("@cls||clear");
}

inline int menu() {
    clear();
    int var;
    fflush(stdin);
    cout << "Welcome to the Subway managing system!" << endl;
    cout << "Choose an option to begin:" << endl;
    cout << "1 - list all orders" << endl;
    cout << "2 - Make an order" << endl;
    cout << "3 - Exit" << endl;
    cin >> var;
    return var;
}

inline int selectOrderType() {
    clear();
    int var;
    cout << "Select an order type:" << endl;
    cout << "1 - Sandwich" << endl;
    cout << "2 - Salad" << endl;
    cin >> var;
    return var;
}

inline int selectBread() {
start:
    clear();
    int var, x;
    cout << "Select the bread:" << endl;
    cout << "1 - Italian" << endl;
    cout << "2 - Parmesan" << endl;
    cin >> var;
    if (var == 1 || var == 2) {
        return var;
    } else {
        cout << "Not an option! Type 0 and enter to try again" << endl;
        cin >> x;
        goto start;
    }
}

inline int selectTopping() {
start:
    clear();
    int var, x;
    cout << "Select the topping:" << endl;
    cout << "1 - Teriaki Chiken" << endl;
    cout << "2 - Barbecue" << endl;
    cin >> var;
    if (var == 1 || var == 2) {
        return var;
    } else {
        cout << "Not an option! Type 0 and enter to try again" << endl;
        cin >> x;
        goto start;
    }
}

inline int selectCheese() {
start:
    clear();
    int var, x;
    cout << "Select the cheese:" << endl;
    cout << "1 - Cheddar" << endl;
    cout << "2 - Swiss" << endl;
    cin >> var;
    if (var == 1 || var == 2) {
        return var;
    } else {
        cout << "Not an option! Type 0 and enter to try again" << endl;
        cin >> x;
        goto start;
    }
}

inline int selectSalads() {
start:
    clear();
    int var, x;
    cout << "Select the salads:" << endl;
    cout << "1 - Lettuce" << endl;
    cout << "2 - Picles" << endl;
    cout << "3 - Tomatoes" << endl;
    cout << "4 - Proceed to sauces" << endl;
    cin >> var;
    if (var == 1 || var == 2 || var == 3 || var == 4) {
        return var;
    } else {
        cout << "Not an option! Type 0 and enter to try again" << endl;
        cin >> x;
        goto start;
    }
}

inline int selectSauces() {
start:
    clear();
    int var, x;
    cout << "Select the sauces:" << endl;
    cout << "1 - Chipotle" << endl;
    cout << "2 - Honey Mustard" << endl;
    cout << "3 - Mayo" << endl;
    cout << "4 - Wrap up order" << endl;
    cin >> var;
    if (var == 1 || var == 2 || var == 3 || var == 4) {
        return var;
    } else {
        cout << "Not an option! Type 0 and enter to try again" << endl;
        cin >> x;
        goto start;
    }
}



#endif