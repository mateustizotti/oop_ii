#ifndef SINGLETON_H
#define SINGLETON_H
#include <iostream>
#include <string>
#include <vector>
#include "order.h"
#include "ITEN.h"

using namespace std;

class Singleton {
    private:
        static Singleton* instance;
        Singleton();
        vector <Order*> nfes;
        vector <Order*>::iterator i;

    public:
        static Singleton* getInstance();
        void addOrder(Order& o);
        void listOrders();
};

#endif