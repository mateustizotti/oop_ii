#ifndef HONEYMOSTARD_H
#define HONEYMOSTARD_H
#include <iostream>
#include <string>
#include "../ingredients.h"

using namespace std;

class HoneyMustard : public Ingredients {
    private:
        float price = 3;
        string name = "Honey Mustard";
    public:
        HoneyMustard(Order *decoratedOrder) : Ingredients(decoratedOrder) {}
        float getPrice() { return m_decoratedOrder->getPrice() + price; }
        string getName() { return m_decoratedOrder->getName() + "\n" + name; }
};

#endif