#ifndef MAYO_H
#define MAYO_H
#include <iostream>
#include <string>
#include "../ingredients.h"

using namespace std;

class Mayo : public Ingredients {
    private:
        float price = 3;
        string name = "Mayo";
    public:
        Mayo(Order *decoratedOrder) : Ingredients(decoratedOrder) {}
        float getPrice() { return m_decoratedOrder->getPrice() + price; }
        string getName() { return m_decoratedOrder->getName() + "\n" + name; }
};

#endif