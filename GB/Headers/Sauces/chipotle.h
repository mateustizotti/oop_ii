#ifndef CHIPOTLE_H
#define CHIPOTLE_H
#include <iostream>
#include <string.h>
#include "../ingredients.h"

using namespace std;

class Chipotle : public Ingredients {
    private:
        float price = 3;
        string name = "Chipotle";
    public:
        Chipotle(Order *decoratedOrder) : Ingredients(decoratedOrder) {}
        float getPrice() { return m_decoratedOrder->getPrice() + price; }
        string getName() { return m_decoratedOrder->getName() + "\n" + name; }
};

#endif