#ifndef SANDWICH_H
#define SANDWICH_H
#include <iostream>
#include <string>
#include "order.h"

using namespace std;

class Sandwich : public Order {
    private:
        float price = 10;
        string name = "Sandwich";
    public:
        float getPrice() { return price; }
        string getName() { return name; }   
};

#endif