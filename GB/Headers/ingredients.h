#ifndef INGREDIENTS_H
#define INGREDIENTS_H
#include <iostream>
#include <string>
#include "order.h"

using namespace std;

class Ingredients : public Order {
    protected:
        Order *m_decoratedOrder;
    public:
        Ingredients(Order *decoratedOrder) : m_decoratedOrder(decoratedOrder) {}
};

#endif