#ifndef FACADE_H
#define FACADE_H
#include <iostream>
#include <string>
#include "utils.h"
#include "order.h"
#include "GUIFactory.h"
#include "Factory.h"
#include "singleton.h"

using namespace std;

class Facade {
    protected:
        Order *temp;
        GuiFactory *guiFactory;
        vector <ITEN> itens;
    public:
        Facade();
        void makeOrder();
        void makeSandwich();
        void makeSalad();
        void copyVector(Order *temp);
};

#endif