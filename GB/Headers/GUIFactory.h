#ifndef GUIFACTORY_H
#define GUIFACTORY_H
#include <iostream>
#include <string>
#include "order.h"

using namespace std;

class GuiFactory {
    public:
        virtual Order *createOrder(char *) = 0;
};

#endif