#ifndef SALAD_H
#define SALAD_H
#include <iostream>
#include <string>
#include "order.h"

using namespace std;

class Salad : public Order {
    private:
        float price = 7;
        string name = "Salad";
    public:
        float getPrice() { return price; }
        string getName() { return name; }
};

#endif