#ifndef LETTUCE_H
#define LETTUCE_H
#include <iostream>
#include <string>
#include "../ingredients.h"

using namespace std;

class Lettuce : public Ingredients {
    private:
        float price = 3;
        string name = "Lettuce";
    public:
        Lettuce(Order *decoratedOrder) : Ingredients(decoratedOrder) {}
        float getPrice() { return m_decoratedOrder->getPrice() + price; }
        string getName() { return m_decoratedOrder->getName() + "\n" + name; }
};

#endif