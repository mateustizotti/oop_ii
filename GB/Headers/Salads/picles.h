#ifndef PICLES_H
#define PICLES_H
#include <iostream>
#include <string>
#include "../ingredients.h"

using namespace std;

class Picles : public Ingredients {
    private:
        float price = 2;
        string name = "Picles";
    public:
        Picles(Order *decoratedOrder) : Ingredients(decoratedOrder) {}
        float getPrice() { return m_decoratedOrder->getPrice() + price; }
        string getName() { return m_decoratedOrder->getName() + "\n" + name; }
};

#endif