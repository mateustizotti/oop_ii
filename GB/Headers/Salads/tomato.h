#ifndef TOMATO_H
#define TOMATO_H
#include <iostream>
#include <string>
#include "../ingredients.h"

using namespace std;

class Tomato : public Ingredients {
    private:
        float price = 4;
        string name = "Tomatoes";
    public:
        Tomato(Order *decoratedOrder) : Ingredients(decoratedOrder) {}
        float getPrice() { return m_decoratedOrder->getPrice() + price; }
        string getName() { return m_decoratedOrder->getName() + "\n" + name; }
};

#endif