#ifndef TERIAKI_H
#define TERIAKI_H
#include <iostream>
#include <string>
#include "../ingredients.h"

using namespace std;

class Teriaki : public Ingredients {
    private:
        float price = 7;
        string name = "Teriaki Chicken";
    public:
        Teriaki(Order *decoratedOrder) : Ingredients(decoratedOrder) {}
        float getPrice() { return m_decoratedOrder->getPrice() + price; }
        string getName() { return m_decoratedOrder->getName() + "\n" + name; }
};

#endif