#ifndef BBQ_H
#define BBQ_H
#include <iostream>
#include <string>
#include "../ingredients.h"

using namespace std;

class Bbq : public Ingredients {
    private:
        float price = 6;
        string name = "Barbecue";
    public:
        Bbq(Order *decoratedOrder) : Ingredients(decoratedOrder) {}
        float getPrice() { return m_decoratedOrder->getPrice() + price; }
        string getName() { return m_decoratedOrder->getName() + "\n" + name; }
};

#endif