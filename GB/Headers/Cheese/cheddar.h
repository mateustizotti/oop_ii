#ifndef CHEDDAR_H
#define CHEDDAR_H
#include <iostream>
#include <string>
#include "../ingredients.h"

using namespace std;

class Cheddar : public Ingredients {
    private:
        float price = 4;
        string name = "Cheddar";
    public:
        Cheddar(Order *decoratedOrder) : Ingredients(decoratedOrder) {}
        float getPrice() { return m_decoratedOrder->getPrice() + price; }
        string getName() { return m_decoratedOrder->getName() + "\n" + name; }
};

#endif