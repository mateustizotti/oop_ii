#ifndef SWISS_H
#define SWISS_H
#include <iostream>
#include <string>
#include "../ingredients.h"

using namespace std;

class Swiss : public Ingredients {
    private:
        float price = 4;
        string name = "Swiss";
    public:
        Swiss(Order *decoratedOrder) : Ingredients(decoratedOrder) {}
        float getPrice() { return m_decoratedOrder->getPrice() + price; }
        string getName() { return m_decoratedOrder->getName() + "\n" + name; }
};

#endif