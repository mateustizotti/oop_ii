#ifndef ITALIAN_H
#define ITALIAN_H
#include <iostream>
#include <string>
#include "../ingredients.h"

using namespace std;

class Italian : public Ingredients {
    private:
        float price = 4;
        string name = "Italian Bread";
    public:
        Italian(Order *decoratedOrder) : Ingredients(decoratedOrder) {}
        float getPrice() { return m_decoratedOrder->getPrice() + price; }
        string getName() { return m_decoratedOrder->getName() + "\n" + name; }
};

#endif