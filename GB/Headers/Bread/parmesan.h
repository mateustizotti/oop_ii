#ifndef PARMESAN_H
#define PARMESAN_H
#include <iostream>
#include <string>
#include "../ingredients.h"

using namespace std;

class Parmesan : public Ingredients {
    private:
        float price = 5;
        string name = "Parmesan Bread";
    public:
        Parmesan(Order *decoratedOrder) : Ingredients(decoratedOrder) {}
        float getPrice() { return m_decoratedOrder->getPrice() + price; }
        string getName() { return m_decoratedOrder->getName() + "\n" + name; }
};

#endif