#ifndef FACTORY_H
#define FACTORY_H
#include <iostream>
#include <string.h>
#include "GUIFactory.h"
#include "sandwich.h"
#include "salad.h"

using namespace std;

class Factory : public GuiFactory {
    public:
        Order *createOrder(char *type) {
            if (strcmp(type, "Sandwich") == 0) {
                return new Sandwich;
            } else if (strcmp(type, "Salad") == 0) {
                return new Salad;
            }
        }
};

#endif