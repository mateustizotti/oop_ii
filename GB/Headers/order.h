#ifndef ORDER_H
#define ORDER_H
#include <iostream>
#include <string>
#include <vector>
#include "ITEN.h"

using namespace std;

class Order {
    protected:
        vector<ITEN> info;
        vector<ITEN>::iterator i;
    public:
        virtual float getPrice() = 0;
        virtual string getName() = 0;
        void addIten(ITEN i) {
            info.push_back(i);
        }
        void listInfo() { 
            for (i = info.begin(); i != info.end(); ++i) {
                cout << i->name << "R$" << i->price << endl;
            }
        }
};

#endif