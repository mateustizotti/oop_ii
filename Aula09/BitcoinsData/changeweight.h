#ifndef CHANGEWEIGHT_H
#define CHANGEWEIGHT_H
#include <QDialog>

namespace Ui {
class changeWeight;
}

class changeWeight : public QDialog
{
    Q_OBJECT

public:
    explicit changeWeight(QWidget *parent = nullptr);
    ~changeWeight();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::changeWeight *ui;
};

#endif // CHANGEWEIGHT_H
