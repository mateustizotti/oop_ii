#include "changehigh.h"
#include "ui_changehigh.h"
#include "data.h"
#include <QDateTime>
#include <QDialog>
#include <QString>

Data *dh = Data::getInstance();

changeHigh::changeHigh(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::changeHigh)
{
    ui->setupUi(this);
    for (int i=0 ; i < dh->getDates().size() ; i++) {
        long double dateEpoch = dh->getDates()[i]+(3600*3);
        QDateTime timestamp;
        timestamp.setTime_t(dateEpoch);
        ui->selectDate->addItem(timestamp.toString());
    }
}

changeHigh::~changeHigh()
{
    delete ui;
}

void changeHigh::on_buttonBox_accepted() {
    int index = ui->selectDate->currentIndex();
    QString value = ui->newHigh->text();

    dh->setNewHigh(index, value.toDouble());
}
