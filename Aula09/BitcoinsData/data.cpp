#include "data.h"

using namespace std;

Data* Data::instance = 0;

Data* Data::getInstance() {
    if (instance == 0) {
        instance = new Data();
    }
    return instance;
}

Data::Data() {

}

void Data::setNewWeight(int index, double value) {
    weighted[index] = value;
}

void Data::setNewHigh(int index, double value) {
    high[index] = value;
}

void Data::setNewLow(int index, double value) {
    low[index] = value;
}
