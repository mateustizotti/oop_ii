#ifndef CHANGELOW_H
#define CHANGELOW_H
#include <QDialog>

namespace Ui {
class changeLow;
}

class changeLow : public QDialog
{
    Q_OBJECT

public:
    explicit changeLow(QWidget *parent = nullptr);
    ~changeLow();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::changeLow *ui;
};

#endif // CHANGELOW_H
