#ifndef CHANGEHIGH_H
#define CHANGEHIGH_H
#include <QDialog>

namespace Ui {
class changeHigh;
}

class changeHigh : public QDialog
{
    Q_OBJECT

public:
    explicit changeHigh(QWidget *parent = nullptr);
    ~changeHigh();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::changeHigh *ui;
};

#endif // CHANGEHIGH_H
