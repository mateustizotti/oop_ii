#include "changeweight.h"
#include "ui_changeweight.h"
#include "data.h"
#include <QDateTime>
#include <QDialog>
#include <QString>

Data *d = Data::getInstance();

changeWeight::changeWeight(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::changeWeight)
{
    ui->setupUi(this);
    for (int i=0 ; i < d->getDates().size() ; i++) {
        long double dateEpoch = d->getDates()[i]+(3600*3);
        QDateTime timestamp;
        timestamp.setTime_t(dateEpoch);
        ui->selectDate->addItem(timestamp.toString());
    }
}

changeWeight::~changeWeight()
{
    delete ui;
}

void changeWeight::on_buttonBox_accepted() {
    int index = ui->selectDate->currentIndex();
    QString value = ui->newWeigth->text();

    d->setNewWeight(index, value.toDouble());
}
