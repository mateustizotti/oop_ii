#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qcustomplot.h"
#include "data.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void plotGrafico();

private slots:
    void on_actionWeights_triggered();

    void on_actionHighs_and_Lows_triggered();

    void on_actionChange_Weight_triggered();

    void on_actionChange_High_triggered();

    void on_actionChange_Low_triggered();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
