#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "data.h"
#include "changeweight.h"
#include "changehigh.h"
#include "changelow.h"
#include <QString>
#include <QDateTime>
#include <time.h>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow) {
    ui->setupUi(this);
    setCentralWidget(ui->grafico);

    QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);
    dateTicker->setDateTimeFormat("HH:mm");
    dateTicker->setTickCount(20);
    ui->grafico->xAxis->setTicker(dateTicker);
    ui->grafico->rescaleAxes(false);

    ui->grafico->xAxis->setBasePen(QPen(Qt::white));
    ui->grafico->xAxis->setTickPen(QPen(Qt::white));
    ui->grafico->xAxis->grid()->setVisible(true);
    ui->grafico->xAxis->grid()->setPen(QPen(QColor(130, 130, 130), 0, Qt::DotLine));
    ui->grafico->xAxis->setTickLabelColor(Qt::white);
    ui->grafico->xAxis->setLabelColor(Qt::white);

    ui->grafico->yAxis->setBasePen(QPen(Qt::white));
    ui->grafico->yAxis->setTickPen(QPen(Qt::white));
    ui->grafico->yAxis->grid()->setVisible(true);
    ui->grafico->yAxis->grid()->setPen(QPen(QColor(130, 130, 130), 0, Qt::DotLine));
    ui->grafico->yAxis->setTickLabelColor(Qt::white);
    ui->grafico->yAxis->setLabelColor(Qt::white);

    QLinearGradient gradient(0, 0, 0, 400);
    gradient.setColorAt(0, QColor(90, 90, 90));
    gradient.setColorAt(0.38, QColor(105, 105, 105));
    gradient.setColorAt(1, QColor(70, 70, 70));
    ui->grafico->setBackground(QBrush(gradient));

    ui->grafico->xAxis->setRange(1601471340+ (3600*2.5), 1601557800+ (3600*2.5));
    ui->grafico->xAxis->setLabel("Dates");
    ui->grafico->xAxis->setLabelColor(QColor("white"));
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_actionWeights_triggered() {
    ui->grafico->clearPlottables();
    ui->grafico->legend->setVisible(false);

    Data *d = Data::getInstance();

    QVector<double> x(25), y(25);
    for (int i = 0 ; i<x.size() ; i++) {
        x[i] = d->getDates()[i] + (3600*2.5);
        y[i] = d->getWeights()[i];
    }

    ui->grafico->addGraph();
    ui->grafico->graph(0)->setData(x, y);
    ui->grafico->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, QPen(Qt::black, 1.5), QBrush(Qt::white), 9));
    ui->grafico->graph(0)->setPen(QPen(QColor(200, 200, 200), 3));

    ui->grafico->xAxis->setLabel("Dates");
    ui->grafico->xAxis->setLabelColor(QColor("white"));
    ui->grafico->yAxis->setLabel("Weights");
    ui->grafico->yAxis->setLabelColor(QColor("white"));

    ui->grafico->xAxis->setTickLabelFont(QFont(QFont().family(), 8));

    ui->grafico->yAxis->setRange(60819.365002, 61545.533295);

    ui->grafico->replot();
}

void MainWindow::on_actionHighs_and_Lows_triggered() {
    ui->grafico->clearPlottables();

    QCPBars *barsHigh = new QCPBars(ui->grafico->xAxis, ui->grafico->yAxis);
    barsHigh->setName("High");
    barsHigh->setPen(QPen(QColor(0, 168, 140).lighter(130)));
    barsHigh->setBrush(QColor(0, 168, 140));

    QCPBars *barsLow = new QCPBars(ui->grafico->xAxis, ui->grafico->yAxis);
    barsLow->setName("Low");
    barsLow->setPen(QPen(QColor(111, 9, 176).lighter(170)));
    barsLow->setBrush(QColor(111, 9, 176));

    Data *d = Data::getInstance();
    QVector<double> xh(25), yh(25);
    for (int i = 0 ; i<xh.size() ; i++) {
        xh[i] = d->getDates()[i] + (3600*3);
        yh[i] = d->getHighs()[i];
    }

    barsHigh->setWidth(2000);
    barsHigh->setData(xh, yh);

    QVector<double> xl(25), yl(25);
    for (int i = 0 ; i<xl.size() ; i++) {
        xl[i] = d->getDates()[i] + (3600*3);
        yl[i] = d->getLows()[i];
    }

    barsLow->setWidth(2000);
    barsLow->setData(xl, yl);

    ui->grafico->legend->setVisible(true);
    ui->grafico->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignTop|Qt::AlignHCenter);
    ui->grafico->legend->setBrush(QColor(255, 255, 255, 100));
    ui->grafico->legend->setBorderPen(Qt::NoPen);
    QFont legendFont = font();
    legendFont.setPointSize(10);
    ui->grafico->legend->setFont(legendFont);
    ui->grafico->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);

    ui->grafico->yAxis->setLabel("");
    ui->grafico->yAxis->setRange(62000, 60500);
    ui->grafico->replot();
}

void MainWindow::on_actionChange_Weight_triggered()
{
    changeWeight dialogChangeWeight;
    dialogChangeWeight.setModal(true);
    dialogChangeWeight.exec();
}

void MainWindow::on_actionChange_High_triggered() {
    changeHigh dialogChangeHigh;
    dialogChangeHigh.setModal(true);
    dialogChangeHigh.exec();
}

void MainWindow::on_actionChange_Low_triggered()
{
    changeLow dialogChangeLow;
    dialogChangeLow.setModal(true);
    dialogChangeLow.exec();
}
