#include "changelow.h"
#include "ui_changelow.h"
#include "data.h"
#include <QDateTime>
#include <QDialog>
#include <QString>

Data *dl = Data::getInstance();

changeLow::changeLow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::changeLow)
{
    ui->setupUi(this);
    for (int i=0 ; i < dl->getDates().size() ; i++) {
        long double dateEpoch = dl->getDates()[i]+(3600*3);
        QDateTime timestamp;
        timestamp.setTime_t(dateEpoch);
        ui->selectDate->addItem(timestamp.toString());
    }
}

changeLow::~changeLow()
{
    delete ui;
}

void changeLow::on_buttonBox_accepted() {
    int index = ui->selectDate->currentIndex();
    QString value = ui->newLow->text();

    dl->setNewLow(index, value.toDouble());
}
