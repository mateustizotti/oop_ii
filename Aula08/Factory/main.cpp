#include <iostream>
#include <string>
#include "Headers/button.h"
#include "Headers/baseFactory.h"
#include "Headers/factory.h"

using namespace std;

int main() {
   BaseFactory* bf;
   Button *btn;

   bf = new Factory; 

   btn = bf->createButton("Windows");
   btn->paint();
   btn = bf->createButton("OSX");
   btn->paint();
   return 0;
}