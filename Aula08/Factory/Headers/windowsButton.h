#ifndef WINDOWSBUTTON_H
#define WINDOWSBUTTON_H
#include <iostream>
#include <string>
#include "button.h"

using namespace std;

class WindowsButton : public Button {
    public:
        void paint();
};

#endif