#ifndef BUTTON_H
#define BUTTON_H
#include <iostream>
#include <string>

using namespace std;

class Button {
    public:
        virtual void paint() = 0;
};

#endif