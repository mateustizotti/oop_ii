#ifndef BASEFACTORY_H
#define BASEFACTORY_H
#include <iostream>
#include <string>
#include "button.h"
#include "windowsButton.h"
#include "osxButton.h"

using namespace std;

class BaseFactory {        
    public:
        virtual Button *createButton(char *) = 0;
};

#endif