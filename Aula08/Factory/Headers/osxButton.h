#ifndef OSXBUTTON_H
#define OSXBUTTON_H
#include <iostream>
#include <string>
#include "button.h"

using namespace std;

class OsxButton : public Button {
    public:
        void paint();
};

#endif