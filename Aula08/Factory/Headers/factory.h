#ifndef FACTORY_H
#define FACTORY_H
#include <iostream>
#include <string.h>
#include "baseFactory.h"

 
using namespace std;

class Factory : public BaseFactory{
    public:
        Button *createButton(char *type) {
            if (strcmp(type, "Windows") == 0) {
                return new WindowsButton;  
            } else if (strcmp(type, "OSX") == 0) {
                return new OsxButton;
            }
        }
};

#endif