#include <iostream>
#include <string>
#include "Headers/singleton.h"

using namespace std;

int main() {
   Singleton *a = Singleton::getInstance();
   Singleton *b = Singleton::getInstance();

   cout << a << endl << b << endl;
   return 0;
}