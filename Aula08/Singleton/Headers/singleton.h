#ifndef SINGLETON_H
#define SINGLETON_H
#include <iostream>
#include <string>

using namespace std;

class Singleton {
    private:
        static Singleton* instance;
        Singleton();
    public:
        static Singleton* getInstance();
};

#endif