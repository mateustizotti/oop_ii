#include <iostream>
#include <string>
#include "Headers/calculator.h"

using namespace std;

void Calculator::calcData(float array[]) {
    float maxi =  max.calcMaximun(array);
    float aveg = avg.calcAverage(array);
    float mini =  min.calcMinimun(array);

    cout << "Maximun : " << maxi << endl;
    cout << "Average :" << aveg << endl; 
    cout << "Minimun :" << mini << endl;
}