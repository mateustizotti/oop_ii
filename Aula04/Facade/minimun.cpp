#include <iostream>
#include <string>
#include "Headers/minimun.h"

using namespace std;

float Minimun::calcMinimun(float array[]) {
    float min = array[0];
    for (int i = 1; i < 9; i++)
    {
        if (array[i] < min)
        {
            min = array[i];
        }
    }
    return min;
}