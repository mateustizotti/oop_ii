#ifndef AVERAGE_H
#define AVERAGE_H
#include <iostream>
#include <string>

using namespace std;

class Average {
    public:
        float calcAverage(float array[]);
};

#endif