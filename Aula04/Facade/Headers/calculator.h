#ifndef CALCULATOR_H
#define CALCULATOR_H
#include <iostream>
#include <string>
#include "maximun.h"
#include "minimun.h"
#include "average.h"

using namespace std;

class Calculator {
    private:
        Maximun max;
        Minimun min;
        Average avg;
    public:
        void calcData(float array[]);
};

#endif