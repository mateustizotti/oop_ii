#include <iostream>
#include <string>
#include "Headers/average.h"

using namespace std;

float Average::calcAverage(float array[]) {
    float total = 0;
    for (int i = 0; i < 9; i++) {
        total += array[i];
    }
    total /= 10;
    return total;
}