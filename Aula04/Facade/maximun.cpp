#include <iostream>
#include <string>
#include "Headers/maximun.h"

using namespace std;

float Maximun::calcMaximun(float array[]) {
    float max = array[0];
    for (int i = 1; i < 9; i++) {
        if (array[i] > max) {
            max = array[i];
        }
    }
    return max;
}