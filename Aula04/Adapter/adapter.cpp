#include <iostream>
#include <string>
#include "Headers/adapter.h"
#include "Headers/legacy.h"

using namespace std;

Adapter::Adapter(int x, int y, int w, int h) {
    l = new Legacy(x, y, x+w, y+h);
}

void Adapter::draw() {
    l->oldDraw();
}