#include <iostream>
#include <string>
#include "Headers/legacy.h"

using namespace std;

Legacy::Legacy() {}

Legacy::Legacy(int xa, int ya, int xb, int yb) {
    x1 = xa;
    y1 = ya;
    x2 = xb;
    y2 = yb;
    cout << "Legacy Rectangle Created\n";
}

void Legacy::oldDraw() {
    cout << "Legacy oldDraw() \n";
}