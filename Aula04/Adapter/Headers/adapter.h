#ifndef ADAPTER_H
#define ADAPTER_H
#include <iostream>
#include <string>
#include "rectangle.h"
#include "legacy.h"

using namespace std;

class Adapter : public Rectangle, private Legacy {
    private:
        Legacy *l;
    public:
        Adapter(int x, int y, int w, int h);
        virtual void draw();
};

#endif