#ifndef LEGACY_H
#define LEGACY_H
#include <iostream>
#include <string>

using namespace std;

class Legacy {
    private:
        int x1, y1, x2, y2;
    public:
        Legacy();
        Legacy(int xa, int ya, int xb, int yb);
        virtual void oldDraw();
};

#endif