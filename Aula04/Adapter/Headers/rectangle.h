#ifndef RECTANGLE_H
#define RECTANGLE_H
#include <iostream>

using namespace std;

class Rectangle {
    public:
        virtual void draw() = 0;
};

#endif