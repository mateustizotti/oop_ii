#include <iostream>
#include <string>
#include "Headers/adapter.h"
#include "Headers/legacy.h"
#include "Headers/rectangle.h"

using namespace std;

int main() {
   int x = 20, y = 50, w = 500, h = 200;
   Rectangle *r = new Adapter(x,y,w,h);
   r->draw();
   return 0;
}