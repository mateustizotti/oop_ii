#include <iostream>
#include <string>
#include <vector>
#include "Headers/subject.h"

using namespace std;

Subject::Subject() {

}

Subject::Subject(int code_, int period_, string description_, string bibliography_, Course parentCourse_) {
    code = code_;
    period = period_;
    description = description_;
    bibliography = bibliography_;
    parentCourses.push_back(parentCourse_);
}

void Subject::listParentCourses() {
    cout << "| Code | Course |" << endl;
    for (int i = 0; i < parentCourses.size(); i++) {
        cout << "| " << parentCourses[i].getCode() << " | " << parentCourses[i].getNameCourse() << " |" << endl;
    }
}

void Subject::addNewParentCourse(Course newParentCourse) {
    parentCourses.push_back(newParentCourse);
}

bool Subject::checkParentCourses(int courseCode) {
    for (int i = 0; i < parentCourses.size(); i++) {
        if (parentCourses[i].getCode() == courseCode) {
            return true;
        }
    }
    return false;
}

bool Subject::checkStudentParentCourse(string cpf) {
    for (int i = 0; i < parentCourses.size(); i++) {
        if (parentCourses[i].checkStudentsInCourse(cpf)) {
            return true;
        } 
    }
    return false;
}