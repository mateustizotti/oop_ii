#include <iostream>
#include <string>
#include <vector>
#include "Headers/utils.h"
#include "Headers/courseSetUp.h"

using namespace std;

CourseSetUp::CourseSetUp() {

}

bool CourseSetUp::courseSetUp(vector <Student> &students, vector <Professor> &teachers) {
    if (students.size() > 0 && teachers.size() > 0) {
        return true;
    } else if(students.size() == 0 && teachers.size() == 0) {
        clear();
        cout << "You need at least one registered Student to create a course!" << endl;
        students.push_back(registerStudent());
        clear();
        cout << "You also need at least one registered Professor to create a course!" << endl;
        teachers.push_back(registerProfessor());
        return true;
    } else if(students.size() == 0) {
        clear();
        cout << "You need at least one registered Student to create a course!" << endl;
        students.push_back(registerStudent());
        return true;
    } else if(teachers.size() == 0) {
        clear();
        cout << "You need at least one registered Professor to create a course!" << endl;
        teachers.push_back(registerProfessor());
        return true;
    }
    return false;
}

void CourseSetUp::linkStudentProfessor(Course &c, vector <Student> &students, vector <Professor> &teachers) {
 menu:   
    switch (linkStudentProfessorMenu(c)) {
    case 1: {
        clear();
        string cpf_;
        int var;
        cout << "Type in the student's CPF: ";
        cin >> cpf_;
        for (int i = 0; i < students.size(); i++) {
            if (students[i].getCpf() == cpf_) {
                if (!c.checkStudentsInCourse(cpf_)) {
                    c.addStudent(&students[i]);
                    cout << students[i].getName() << " was added to the course! Type 0 to return" << endl;
                    cin >> var;
                    goto menu;
                } else {
                    cout << "This Student is already added to this course! Type 0 to return" << endl;
                    cin >> var;
                    goto menu;
                }
            }
        }
        cout << "CPF not found! Type 0 to return and try again" << endl;
        cin >>var;
        goto menu;        
    }
    case 2: {
        clear();
        string cpf_;
        int var;
        cout << "Type in the Professor's CPF: ";
        cin >> cpf_;
        for (int i = 0; i < teachers.size(); i++) {
            if (cpf_ == teachers[i].getCpf()) {
                if (!c.checkProfessorsInCourse(cpf_)) {
                    c.addProfessor(&teachers[i]);
                    cout << teachers[i].getName() << " was added to the course! Type 0 to return" << endl;
                    cin >> var;
                    goto menu;
                } else {
                    cout << "This professor is already added to this course! Type 0 to return" << endl;
                    cin >> var;
                    goto menu;
                }
            } 
        }
        cout << "CPF not found! Type 0 to return and try again" << endl;
        cin >>var;
        goto menu;
    }
    case 3: {
        break;
    }
    default:
        break;
    }
}