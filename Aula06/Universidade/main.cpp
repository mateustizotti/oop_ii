#include <iostream>
#include <string>
#include <vector>
#include "Headers/person.h"
#include "Headers/student.h"
#include "Headers/professor.h"
#include "Headers/course.h"
#include "Headers/courseSetUp.h"
#include "Headers/subject.h"
#include "Headers/turma.h"
#include "Headers/classSetUp.h"
#include "Headers/utils.h"

using namespace std;

int main() {
   int var, cn, subjectCode, classCode;
   string cpf;
   vector <Student> students;
   vector <Professor> teachers;
   vector <Course> courses;
   vector <Subject> subjects;
   vector <Turma> classes;

 menu:  
   switch (menu()) {
   case 1:
      switch (studentsMenu()) {
      case 1:
         clear();
         students.push_back(registerStudent());
         goto menu;
      case 2:
         clear();
         listStudents(students);
         goto menu;
      case 3:
         goto menu;   
      }
   case 2:
      switch (professorsMenu()) {
      case 1:
         clear();
         teachers.push_back(registerProfessor());
         goto menu;
      case 2:
         clear();
         listProfessors(teachers);
         goto menu;
      case 3:
         goto menu;
      }
   case 3:
      switch (courseMenu()) {
      case 1: {
         clear();
         CourseSetUp *cs = new CourseSetUp();
         if(cs->courseSetUp(students, teachers)) {
            clear();
            fflush(stdin);
            Course c = registerCourse();
            clear();
            cout << "You are required to link a Student and a Professor to the Course!" << endl;
            cout << "Type 0 to proceed: ";
            cin >> var;
            cs->linkStudentProfessor(c, students, teachers);
            courses.push_back(c);
            goto menu;
         }
      }
      case 2: {
         clear();
         listCourses(courses);
         goto menu;
      }
      case 3: {
         clear();
         cout << "Type in the course code: ";
         cin >> cn;
         for (int i = 0; i < courses.size(); i++) {
            if (courses[i].getCode() == cn) {
               if (courses[i].checkStudents()) {
                  courses[i].listStudentsInCourse();
                  cout << endl;
                  cout << "Type 0 to return to main menu" << endl;
                  cin >> var;
                  goto menu;
               } else {
                  cout << "There are no students enrolled in this course yet!" << endl;
                  cout << "Type 0 to return to main menu" << endl;
                  cin >> var;
               goto menu;
               }
            } else {
               cout << "Course not found! Type 0 to return" << endl;
               cin >> var;
               goto menu;
            }  
         }
      }
      case 4: {
         clear();
         cout << "Type in the course code: ";
         cin >> cn;
         for (int i = 0; i < courses.size(); i++) {
            if (courses[i].getCode() == cn) {
               if (courses[i].checkProfessors()) {
                  courses[i].listProfessorsInCourse();
                  cout << endl;
                  cout << "Type 0 to return to main menu" << endl;
                  cin >> var;
                  goto menu;
               } else {
                  cout << "There are no Professorrs linked in this course yet!" << endl;
                  cout << "Type 0 to return to main menu" << endl;
                  cin >> var;
                  goto menu;
               }
            } else {
               cout << "Course not found! Type 0 to return" << endl;
               cin >> var;
               goto menu;
            }  
         }
      }
      case 5: {
         clear();
         cout << "Type in the course code: ";
         cin >> cn;
         for (int i = 0; i < courses.size(); i++) {
            if (courses[i].getCode() == cn) {
               CourseSetUp *cs = new CourseSetUp();
               cs->linkStudentProfessor(courses[i], students, teachers);
               goto menu;
            } else {
               cout << "Course not found! Type 0 to return" << endl;
               cin >> var;
               goto menu;
            }
         }
      }
      case 6: {
         goto menu;
         }
      }
   case 4:
      switch (subjectMenu()) {
      case 1: {
         clear();
         subjects.push_back(registerSubject(courses));
         goto menu;
      }
      case 2: {
         clear();
         listSubjects(subjects);
         goto menu;
      }
      case 3: {
         clear();
         cout << "Type in the subject's code: ";
         cin >> subjectCode;
         listParent(subjectCode, subjects);
         goto menu;
      }
      case 4: {
         clear();
         cout << "Type in the subject's code: ";
         cin >> subjectCode;
         for (int i = 0; i < subjects.size(); i++) {
            if (subjects[i].getCode() == subjectCode) {
               cout << "Type in the course's code: ";
               cin >> cn;
               for (int j = 0; j < courses.size(); j++) {
                  if (courses[j].getCode() == cn) {
                     if (!subjects[i].checkParentCourses(cn)) {
                        subjects[i].addNewParentCourse(courses[j]);
                        cout << courses[j].getNameCourse() << " was added to the subject " << subjects[i].getSubjectDescription() << "!" << endl;
                        cout << "Type 0 to return" << endl;
                        cin >> var;
                        goto menu; 
                     } else {
                        cout << courses[j].getNameCourse() <<" is already associated with " << subjects[i].getSubjectDescription() << "\nType 0 to return" << endl;
                        cin >> var;
                        goto menu;
                     }
                  }   
               }
               cout << "Course not found! Type 0 to return" << endl;
               cin >> var;
               goto menu;
            }
         }
         cout << "Subject not found! Type 0 to return" << endl;
         cin >> var;
         goto menu;
      }
      case 5: {
         goto menu;
      }
      }

   case 5:{
      switch (classMenu()) {
      case 1: {
         clear();
         ClassSetUp *cs = new ClassSetUp();
         Turma c = cs->registerClassFacade(teachers, subjects);
         classes.push_back(c);
         goto menu;
      }
      case 2: {
         clear();
         listClasses(classes);
         goto menu;
      }
      case 3: {
         clear();
         cout << "Type in the class' code: ";
         cin >> classCode;
         cout << endl;    
         for (int i = 0; i < classes.size(); i++) {
            if (classes[i].getClassCode() == classCode) {
               cout << "There are currently " << classes[i].getOpenSpots() << " open spots for this class" << endl;
               cout << endl;    
               cout << "Type 0 to return" << endl;
               cin >> var;
               goto menu;
            }
         }
         cout << "Class not found! Type 0 to return" << endl;
         cin >> var;
      }
      case 4: {
      clear();
         cout << "Type in the class' code: ";
         cin >> classCode;
         for (int i = 0; i < classes.size(); i++) {
            if (classes[i].getClassCode() == classCode) {
               cout << "Type in the Student's cpf: ";
               cin >> cpf;
               for (int j = 0; j < students.size(); j++) {
                  if (students[j].getCpf() == cpf) {
                     if (classes[i].isStudentInCourse(students[j])) {
                        classes[i].enrollStudent(students[j]);
                        cout << endl;    
                        cout << students[j].getName() << " is now enrolled in " << classes[i].getClassDescription() << endl;
                        cout << "Type 0 to return" << endl;
                        cin >> var;
                        goto menu;
                     }
                     cout << "Student is not enrolled in any parent course for this class's subject!" << endl;
                     cout << "Type 0 to return" << endl;
                     cin >> var;
                     goto menu;
                  }
               }   
               cout << "CPF not found! Type 0 to return" << endl;
               cin >> var;
               goto menu;
            }
            cout << "Class not found! Type 0 to return" << endl;
            cin >> var;
            goto menu;
         }
      }
      case 5: {
         goto menu;
      }
      }
   }
   case 6:
      clear();
      cout << "Exiting...\n";
      cout << "Bye bye!\n";
      return 0;   
   } 

   return 0;
}