#include <iostream>
#include <string>
#include <vector>
#include "Headers/turma.h"

using namespace std;

Turma::Turma() {

}

Turma::Turma(int code_, int year_, int semester_, string description_, int maxStudents_, string time_, Subject &pSubj) {
    code = code_;
    year = year_;
    semester = semester_;
    description = description_;
    maxStudents = maxStudents_;
    time = time_;
    parentSubject = pSubj;
}

int Turma::getOpenSpots() {
    return maxStudents - studentsInClass.size();
}

void Turma::enrollStudent(Student &student) {
    studentsInClass.push_back(student);
}

void Turma::associateProfessor(vector <Professor> &teachers) {
    string cpf, var;
    cout << endl;    
    cout << "Every class requires a professor!" << endl;
start:
    cout << "Type in the professor's cpf: ";
    cin >> cpf;
    cout << endl;    
    for (int i = 0; i < teachers.size(); i++) {
        if (teachers[i].getCpf() == cpf) {
            prof = teachers[i];
            cout << teachers[i].getName() << " was associated with this class!" << endl;
            goto end;
        }
    }
    cout << "CPF not found! Try again" << endl;
    goto start;
end:
    cout << "Type 0 to continue" << endl;
    cin >> var;
}

bool Turma::isStudentInCourse(Student &c) {
    if (parentSubject.checkStudentParentCourse(c.getCpf())) {
        return true;
    }
    return false; 
}