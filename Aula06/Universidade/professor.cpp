#include "Headers/professor.h"

Professor::Professor()
{
  registration  = 0;
  maxTitulation = "";
  contractType  = ""; 
  benefits       = 0.0;
};

Professor::~Professor()
{
  //destrutor
};


Professor::Professor(string name_, string birthd_, string address_, string phone_, string cpf_, string rg_, int registration_, string maxTitulation_, string contractType_, float benefits_) :
  Person (name_, birthd_, address_, phone_, cpf_, rg_)
{
  registration   = registration_;
  maxTitulation  = maxTitulation_;
  contractType   = contractType_;
  benefits        = benefits_;
};

void Professor::showData()
{
  cout << "Name: "            << name           << endl;
  cout << "Birthday: "        << birthd         << endl;
  cout << "Adress: "          << address        << endl;
  cout << "Phone: "           << phone          << endl;
  cout << "CPF: "             << cpf            << endl;
  cout << "RG: "              << rg             << endl;
  cout << "Registration: "    << registration   << endl;
  cout << "Max titulation: "  << maxTitulation  << endl;
  cout << "Contract type: "   << contractType   << endl;
  cout << "Benefits: "         << benefits        << endl;
};