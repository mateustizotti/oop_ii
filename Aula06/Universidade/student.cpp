#include <iostream>
#include "Headers/student.h"

using namespace std;

Student::Student() {

};

Student::Student(string name_, string birthd_, string address_, string phone_, string cpf_, string rg_, int enroll_, int firstYear_, int firstSemester_, string status_) : 
    Person(name_, birthd_, address_, phone_, cpf_, rg_) {
        enroll = enroll_;
        firstYear = firstYear_;
        firstSemester = firstSemester_;
        status = status_;
}

void Student::showData() { 
    cout << "Name: " << name << endl;
    cout << "Birthday: " << birthd << endl;
    cout << "Address: " << address << endl;
    cout << "Phone: " << phone << endl;
    cout << "CPF: " << cpf << endl;
    cout << "RG: " << rg << endl;
    cout << "Enrollment Number: " << enroll << endl;
    cout << "First Year: " << firstYear << endl;
    cout << "First Semester: " << firstSemester << endl;
    cout << "Status: " << status << endl;
}