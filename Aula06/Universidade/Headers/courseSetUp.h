#ifndef COURSESETUP_H
#define COURSESETUP_H
#include <iostream>
#include <string>
#include <vector>
#include "student.h"
#include "professor.h"
#include "course.h"

using namespace std;

class CourseSetUp {
    private:

        
    public:
        CourseSetUp();
        bool courseSetUp(vector <Student> &students, vector <Professor> &teachers);
        void linkStudentProfessor(Course &c, vector <Student> &students, vector <Professor> &teachers);
};

#endif