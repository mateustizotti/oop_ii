#ifndef CLASSSETUP_H
#define CLASSSETUP_H
#include <iostream>
#include <string>
#include <vector>
#include "turma.h"
#include "utils.h"

using namespace std;

class ClassSetUp {
    protected:
        Turma c;
    public:
        ClassSetUp();
        Turma registerClassFacade(vector <Professor> teachers, vector <Subject> subjects);
};

#endif