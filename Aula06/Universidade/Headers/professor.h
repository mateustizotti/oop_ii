#ifndef PROFESSOR_H
#define PROFESSOR_H
#include <iostream>
#include <string>

#include "person.h"

using namespace std;

class Professor : public Person{
    private:
        int registration;
        string maxTitulation, contractType;
        float benefits;
    public:
        Professor();
        ~Professor();
        Professor(string name_, string birthd_, string address_, string phone_, string cpf_, string rg_, 
                    int registration_, string maxTitulation_, string contractType_, float benefits_);

        void showData();
};

#endif