#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <string.h>
#include <vector>
#include "person.h"
#include "student.h"
#include "professor.h"
#include "course.h"
#include "subject.h"
#include "turma.h"

using namespace std;

inline void clear() {
    system("@cls||clear");
}

inline int menu() {
    int menu;
    clear();
    fflush(stdin);
    cout << "Welcome to the University management system!" << endl; 
    cout << "Select the desired option:" << endl;
    cout << "1 - Manage Students" << endl;
    cout << "2 - Manage Professors" << endl;
    cout << "3 - Manage Courses" << endl;
    cout << "4 - Manage Subjects" << endl;
    cout << "5 - Manage Classes" << endl;
    cout << "6 - Exit" << endl;
    cin >> menu;
    return menu;
}

inline int studentsMenu() {
    int menu;
    clear();
    fflush(stdin);
    cout << "Select the desired option:" << endl;
    cout << "1 - Add a Students" << endl;
    cout << "2 - List all Students" << endl;
    cout << "3 - Return to main menu" << endl;
    cin >> menu;
    return menu;
}

inline int professorsMenu() {
    int menu;
    clear();
    fflush(stdin);
    cout << "Select the desired option:" << endl;
    cout << "1 - Add a Professor" << endl;
    cout << "2 - List all Professors" << endl;
    cout << "3 - Return to main menu" << endl;
    cin >> menu;
    return menu;
}

inline int courseMenu() {
    int menu;
    clear();
    fflush(stdin);
    cout << "Select the desired option:" << endl;
    cout << "1 - Add a new course" << endl;
    cout << "2 - List all Courses" << endl;
    cout << "3 - List all Students in a Course" << endl;
    cout << "4 - List all Professors in a Course" << endl;
    cout << "5 - Link Students and/or Professors to a Course" << endl;
    cout << "6 - Return to main menu" << endl;
    cin >> menu;
    return menu;
}

inline int subjectMenu() {
    int menu;
    clear();
    fflush(stdin);
    cout << "Select the desired option:" << endl;
    cout << "1 - Add a Subject" << endl;
    cout << "2 - List all Subjects" << endl;
    cout << "3 - List all Courses linked to a Subject" << endl;
    cout << "4 - Link another Course to a Subject" << endl;
    cout << "5 - Return to main menu" << endl;
    cin >> menu;
    return menu;
}

inline int classMenu() {
    int menu;
    clear();
    fflush(stdin);
    cout << "Select the desired option:" << endl;
    cout << "1 - Add a Class" << endl;
    cout << "2 - List all Classes" << endl;
    cout << "3 - Check a Class for open spots" << endl;
    cout << "4 - Enroll a Student into a Class" << endl;
    cout << "5 - Return to main menu" << endl;
    cin >> menu;
    return menu;
}

inline int linkStudentProfessorMenu(Course &c) {
    int menu;
    clear();
    fflush(stdin);
    cout << "Select the desired option:" << endl;
    cout << "1 - Link a Student" << endl;
    cout << "2 - Link a Professor" << endl;
    cout << "3 - Return to main menu" << endl;
    cin >> menu;
    return menu;
}

inline Student registerStudent() {
   string name, birthd, address, phone, cpf, rg, status;
   int enroll, firstYear, firstSemester, var;
   cout << "Type in the student's data\n" << endl;
   cout << "Name: ";
   cin >> name;
   cout << endl;
   fflush(stdin);
   cout << "Birthday: ";
   cin >> birthd;
   cout << endl;
   fflush(stdin);
   cout << "Address: ";
   cin >> address;
   cout << endl;
   fflush(stdin);
   cout << "Phone number: ";
   cin >> phone;
   cout << endl;
   fflush(stdin);
   cout << "CPF: ";
   cin >> cpf;
   cout << endl;
   fflush(stdin);
   cout << "RG: ";
   cin >> rg;
   cout << endl;
   fflush(stdin);
   cout << "Enrollment number: ";
   cin >> enroll;
   cout << endl;
   fflush(stdin);
   cout << "First year: ";
   cin >> firstYear;
   cout << endl;
   fflush(stdin);
   cout << "First semester: ";
   cin >> firstSemester;
   cout << endl;
   fflush(stdin);
   cout << "Current status: ";
   cin >> status;
   cout << endl;
   fflush(stdin);
   cout << "Added new Student! Type 0 to return to main menu" << endl;
   cin >> var;
   Student *a = new Student(name, birthd, address, phone, cpf, rg, enroll, firstYear, firstSemester, status);
   return *a;
}

inline void listStudents(vector <Student> students) {
    int var;
    if (students.size() > 0) {
            for (int i = 0; i < students.size(); i++) {
               cout << "--- Student " << i+1 << " ---" << endl;
               students[i].showData();
               cout << endl;
            }
            cout << "Type 0 to return to main menu" << endl;
            cin >> var;
         } else {
            cout << "There are no registered students yet! Type 0 to return to main menu" << endl;
            cin >> var;
         }
}

inline Professor registerProfessor() {
   string name, birthd, address, phone, cpf, rg, maxTitulation, contractType;
   int registration, var;
   float benefits;
   cout << "Type in the professor's data\n" << endl;
   cout << "Name: ";
   cin >> name;
   cout << endl;
   fflush(stdin);
   cout << "Birthday: ";
   cin >> birthd;
   cout << endl;
   fflush(stdin);
   cout << "Address: ";
   cin >> address;
   cout << endl;
   fflush(stdin);
   cout << "Phone number: ";
   cin >> phone;
   cout << endl;
   fflush(stdin);
   cout << "CPF: ";
   cin >> cpf;
   cout << endl;
   fflush(stdin);
   cout << "RG: ";
   cin >> rg;
   cout << endl;
   fflush(stdin);
   cout << "Registration number: ";
   cin >> registration;
   cout << endl;
   fflush(stdin);
   cout << "Titulation: ";
   cin >> maxTitulation;
   cout << endl;
   fflush(stdin);
   cout << "Contract type: ";
   cin >> contractType;
   cout << endl;
   fflush(stdin);
   cout << "Benefits: ";
   cin >> benefits;
   cout << endl;
   fflush(stdin);
   cout << "Added new Professor! Type 0 to return to main menu" << endl;
   cin >> var;
   Professor *p = new Professor(name, birthd, address, phone, cpf, rg, registration, maxTitulation, contractType, benefits);
   return *p;
}

inline void listProfessors(vector <Professor> teachers) {
    int var;
    if (teachers.size() > 0) {
        for (int i = 0; i < teachers.size(); i++) {
            cout << "--- Professor " << i+1 << " ---" << endl;
            teachers[i].showData();
            cout << endl;
        }
        cout << "Type 0 to return to main menu" << endl;
        cin >> var;
    } else {
        cout << "There are no registered professors yet! Type 0 to return to main menu" << endl;
        cin >> var;
    }
}

inline Course registerCourse() {
    string name, type;
    int code, hours, periods, var;
    fflush(stdin);
    cout << "Type in the course's info\n" << endl;
    cout << "Code: ";
    cin >> code;
    cout << endl;
    fflush(stdin);
    cout << "Name: ";
    cin >> name;
    cout << endl;
    fflush(stdin);
    cout << "Hours: ";
    cin >> hours;
    cout << endl;
    fflush(stdin);
    cout << "Periods: ";
    cin >> periods;
    cout << endl;
    fflush(stdin);
    cout << "Type: ";
    cin >> type;
    cout << endl;
    fflush(stdin);
    cout << "Added new Course! Type 0 to return to main menu" << endl;
    cin >> var;
    Course *c = new Course(code, name, hours, periods, type);
    return *c;
}

inline void listCourses(vector <Course> courses) {
    int var;
    if (courses.size() > 0) {
        cout << "| Code | Course |" << endl;
        for (int i = 0; i < courses.size(); i++) {
            cout << "| " << courses[i].getCode() << " | " << courses[i].getNameCourse() << " |" << endl;
            cout << endl;
        }
        cout << "Type 0 to return to main menu" << endl;
        cin >> var;
    } else {
        cout << "There are no registered courses yet! Type 0 to return to main menu" << endl;
        cin >> var;
    }
}

inline Subject registerSubject(vector <Course> &courses) {
    int code, period, parentCourseCode, var;
    string description, bibliography;
    Course parentCourse;
    fflush(stdin);
    cout << "Type in the subject's info\n" << endl;
    cout << "Code: ";
    cin >> code;
    cout << endl;
    fflush(stdin);
    cout << "Period: ";
    cin >> period;
    cout << endl;
    fflush(stdin);
    cout << "Description: ";
    cin >> description;
    cout << endl;
    fflush(stdin);
    cout << "Bibliography: ";
    cin >> bibliography;
    cout << endl;
course:
    cout << "Parent Course's code: ";
    cin >> parentCourseCode;
    for (int i = 0; i < courses.size(); i++) {
        if (courses[i].getCode() == parentCourseCode) {
            parentCourse = courses[i];
            cout << courses[i].getNameCourse() << " was associated to this subject!" << endl;
            cout << "Added new Subject! Type 0 to return to main menu" << endl;
            cin >> var;
            Subject *s = new Subject(code, period, description, bibliography, parentCourse);
            return *s;
        }
    }
    cout << "Course not found! Try again" << endl;
    goto course;   
}

inline void listSubjects(vector <Subject> subjects) {
    int var;
    if (subjects.size() > 0) {
        for (int i = 0; i < subjects.size(); i++) {
            cout << "| Code | Subject |" << endl;
            cout << "| " << subjects[i].getCode() << " | " << subjects[i].getSubjectDescription() << " |" << endl;
            cout << endl;
        }
        cout << "Type 0 to return to main menu" << endl;
        cin >> var;
    } else {
        cout << "There are no registered subjects yet! Type 0 to return to main menu" << endl;
        cin >> var;
    }
}

inline void listParent(int subjectCode, vector <Subject> subjects) {
    int var;
    for (int i = 0; i < subjects.size(); i++) {
        if (subjects[i].getCode() == subjectCode) {
            cout << "Subject - " << subjects[i].getSubjectDescription() << endl;
            subjects[i].listParentCourses();
            cout << "Type 0 to return" << endl;
            cin >> var;
            goto end;
        }  
    }
    cout << "Course not found! Type 0 to return" << endl;
    cin >> var;
end:
    cout << "";
}

inline Turma registerClass(vector <Subject> &subjects) {
    int code, year, semester, maxStudents, subjCode, var;
    string description, time;
    Subject pSubj;
    
    fflush(stdin);
    cout << "Type in the class's info\n" << endl;
    cout << "Code: ";
    cin >> code;
    cout << endl;
    fflush(stdin);
    cout << "Year: ";
    cin >> year;
    cout << endl;
    fflush(stdin);
    cout << "Semester: ";
    cin >> semester;
    cout << endl;
    fflush(stdin);
    cout << "Description: ";
    cin >> description;
    cout << endl;
    fflush(stdin);
    cout << "Maximum number of students: ";
    cin >> maxStudents;
    cout << endl;
    fflush(stdin);
    cout << "Time: ";
    cin >> time;
    cout << endl;
    fflush(stdin);
    cout << "Every class requires a parent Subject!" << endl;
subject:
    cout << endl;    
    cout << "Type in the subject's code ";
    cin >> subjCode;
    cout << endl;    
    for (int i = 0; i < subjects.size(); i++) {
        if (subjects[i].getCode() == subjCode) {
            pSubj = subjects[i];
            cout << subjects[i].getSubjectDescription() << " was associated with this class!" << endl;
            cout << "Type 0 to continue" << endl;
            cin >> var;
            Turma *c = new Turma(code, year, semester, description, maxStudents, time, pSubj);
            return *c;
        }
    }
    cout << "Subject not found! Try again" << endl;
    goto subject; 
}

inline void listClasses(vector <Turma> classes) {
    int var;
    cout << "| Code | Description | Professor |" << endl;
    for (int i = 0; i < classes.size(); i++) {
        cout << "| " << classes[i].getClassCode() << " | " << classes[i].getClassDescription() << " | " << classes[i].getProfessorName() << " |" << endl;
    }
    cout << "Type 0 to return" << endl;
    cin >> var;
}

#endif