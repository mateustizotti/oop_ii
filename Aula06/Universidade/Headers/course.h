#ifndef COURSE_H
#define COURSE_H
#include <iostream>
#include <string>
#include <vector>
#include "student.h"
#include "professor.h"

using namespace std;

class Course {
    protected:
        int code, hours, periods;
        string name, type;
        vector<Student> studentsInCourse;
        vector<Professor> professorsInCourse;

    public:
        Course();
        Course(int code_, string name_, int hours_, int periods_, string type_);
        string getNameCourse() { return name; }
        int getCode() { return code; }
        void addStudent(Student *s);
        void addProfessor(Professor *p);
        void listStudentsInCourse();
        void listProfessorsInCourse();
        bool checkStudents();
        bool checkProfessors();
        bool checkProfessorsInCourse(string cpf_);
        bool checkStudentsInCourse(string cpf_);
};

#endif