#ifndef SUBJECT_H
#define SUBJECT_H
#include <iostream>
#include <string>
#include <vector>
#include "course.h"

using namespace std;

class Subject {
    protected:
        int code, period;
        string description, bibliography;
        vector <Course> parentCourses;
    public:
    Subject();
    Subject(int code_, int period_, string description_, string bibliography_, Course parentCourse_);
    int getCode() { return code; }
    string getSubjectDescription() { return description; }
    void listParentCourses();
    void addNewParentCourse(Course newParentCourse);
    bool checkParentCourses(int courseCode);
    bool checkStudentParentCourse(string cpf);
};

#endif