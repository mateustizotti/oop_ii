#ifndef STUDENT_H
#define STUDENT_H
#include <iostream>
#include <string>
#include "person.h"

using namespace std;

class Student : public Person {
    private:
        int enroll, firstYear, firstSemester;
        string status;
    public:
        Student();
        Student(string name_, string birthd_, string address_, string phone_, string cpf_, string rg_, int enroll, int firstYear, int firstSemester, string status);
        void showData(void);
};

#endif