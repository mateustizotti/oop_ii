#ifndef TURMA_H
#define TURMA_H
#include <iostream>
#include <string>
#include <vector>
#include "professor.h"
#include "student.h"
#include "subject.h"

using namespace std;

class Turma {
    protected:
        int code, year, semester, maxStudents;
        string description, time;
        Professor prof;
        vector <Student> studentsInClass;
        Subject parentSubject;
    public:
        Turma();
        Turma(int code_, int year_, int semester_, string description_, int maxStudents, string time, Subject &pSubj);
        int getMaxStudents() { return maxStudents; }
        int getClassCode() { return code; }
        int getOpenSpots();
        string getProfessorName() { return prof.getName(); }
        string getClassDescription() { return description; }
        void enrollStudent(Student &student);
        void associateProfessor(vector <Professor> &teachers);
        bool isStudentInCourse(Student &s);
};

#endif