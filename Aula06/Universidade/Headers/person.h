#ifndef PERSON_H
#define PERSON_H
#include <iostream>
#include <string>

using namespace std;

class Person {
    protected:
        string name, birthd, address, phone, cpf, rg; 
        
    public:
    Person();
    Person(string name_, string birthd_, string address_, string phone_, string cpf_, string rg_);
    string getName() { return name; }
    string getCpf() { return cpf; }

    virtual void showData() = 0;
};

#endif