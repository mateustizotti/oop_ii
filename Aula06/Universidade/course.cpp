#include <iostream>
#include <string>
#include "Headers/course.h"

using namespace std;

Course::Course() {

};

Course::Course(int code_, string name_, int hours_, int periods_, string type_) {
    code = code_;
    name = name_;
    hours = hours_;
    periods = periods_;
    type = type_;
};

void Course::addStudent(Student *s) {
    studentsInCourse.push_back(*s);
}

void Course::addProfessor(Professor *p) {
    professorsInCourse.push_back(*p);
}

void Course::listStudentsInCourse() {
    for (int i = 0; i < studentsInCourse.size(); i++) {
        cout << studentsInCourse[i].getName() << endl;
    }
}

void Course::listProfessorsInCourse() {
    for (int i = 0; i < professorsInCourse.size(); i++) {
        cout << professorsInCourse[i].getName() << endl;
    }
}

bool Course::checkStudents() {
    if (studentsInCourse.size() > 0) {
        return true;
    }
    return false;
}

bool Course::checkProfessors() {
    if (professorsInCourse.size() > 0) {
        return true;
    }
    return false;
}

bool Course::checkProfessorsInCourse(string cpf_) {
    for (int i = 0; i < professorsInCourse.size(); i++) {
        if (professorsInCourse[i].getCpf() == cpf_) {
            return true;
        } 
    }
    return false;
}

bool Course::checkStudentsInCourse(string cpf_) {
    for (int i = 0; i < studentsInCourse.size(); i++) {
        if (studentsInCourse[i].getCpf() == cpf_) {
            return true;
        } 
    }
    return false;
}