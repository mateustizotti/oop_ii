#include <iostream>
#include "Headers/grades.h"

using namespace std;

int main() {
    float grauA, grauB, avg, GC;
    int choice;

    cout << "Welcome to the avarage calculation system!" << endl;
    cout << "GA: ";
    cin >> grauA;
    cout << "GB: ";
    cin >> grauB;

    Grades *g = new Grades(grauA, grauB);

    cout << "Your grades:" << endl;
    g->print();
    cout << endl;

    avg = g->getAvg(*g);

    cout << "Your Avarage: " << avg << endl;
    
    if (avg < 6) {
menu:
        cout << "Avarage bellow 6! Which grade would you like to retake?" << endl;
        cout << "GA -> 1" << endl;
        cout << "GB -> 2" << endl;
        cin >> choice;
        switch (choice) {
        case 1:
            GC = 18 - (2*g->getGB());
            cout << "You need to score at least " << GC << " on the GC!" << endl;
            break;
        case 2:
            GC = (18 - g->getGA()) / 2;
            cout << "You need to score at least " << GC << " on the GC!" << endl;
            break;
        default:
            cout << "Not an option! Try again" << endl;
            goto menu;
            break;
        }
    } else {
        cout << "Yay! You are approved!" << endl;
    }

    return 0;
}