#include "Headers/grades.h"

using namespace std;

Grades::Grades() {

}

Grades::Grades(float grauA, float grauB) {
    GA = grauA;
    GB = grauB;
};

void Grades::setGA(float grauA) {
    GA = grauA;
};

void Grades::setGB(float grauB) {
    GB = grauB;
};

void Grades::print() {
    cout << "GA: " << GA << endl;
    cout << "GB: " << GB << endl;
};

float Grades::getAvg(Grades g) {
    float avg;
    avg = ( (g.GA) + (2*g.GB) ) / 3;
    return avg;
};