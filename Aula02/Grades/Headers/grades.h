#ifndef GRADES_H
#define GRADES_H
#include <iostream>

using namespace std;

class Grades {
    private:
        float GA, GB;
    public:
    Grades();
    Grades(float grauA, float grauB);
    
    float getGA() {return GA;}
    float getGB() {return GB;}

    void setGA(float grauA);
    void setGB(float grauB);

    void print(void);

    float getAvg(Grades g);
};

#endif