#include <iostream>
#include <string>

#include "Headers/pizza.h"
#include "Headers/salty.h"
#include "Headers/sweet.h"
#include "Headers/extra.h"

#include "Headers/saltyExtras/cheese.h"
#include "Headers/saltyExtras/peperoni.h"
#include "Headers/saltyExtras/onion.h"
#include "Headers/saltyExtras/pepper.h"

#include "Headers/sweetExtras/strawberry.h"
#include "Headers/sweetExtras/condMilk.h"
#include "Headers/sweetExtras/chocolate.h"
#include "Headers/sweetExtras/sprinkles.h"

using namespace std;

void clear() {
    system("@cls||clear");
}

int menu() { 
    clear();
    int x;
    cout << "What kind of Pizza do you want?" << endl;
    cout << "1 - Salty" << endl;
    cout << "2 - Sweet" << endl;
    cin >> x;
    return x;
}

int saltyMenu() {
    clear();
    int x;
    cout << "Select which extras you want:" << endl;
    cout << "1 - Cheese" << endl;
    cout << "2 - Peperoni" << endl;
    cout << "3 - Onions" << endl;
    cout << "4 - Pepper" << endl;
    cout << "5 - Finish order" << endl;
    cin >> x;
    return x;
}

int sweetMenu() {
    clear();
    int x;
    cout << "Select which extras you want:" << endl;
    cout << "1 - Chocolate" << endl;
    cout << "2 - Condensed Milk" << endl;
    cout << "3 - Strawberrys" << endl;
    cout << "4 - Sprinkles" << endl;
    cout << "5 - Finish order" << endl;
    cin >> x;
    return x;
}


int main() {
    int x, salt, sweet, var;
    Pizza *temp;
    x = menu();
menu:
    switch (x)
    {
    case 1: {
        temp = new Salty();
    salty:
        salt = saltyMenu();
        switch (salt) {
            case 1: {
                temp = new Cheese(temp);
                cout << "Added Cheese! Type 0 and enter to continue" << endl;
                cin >> var;
                goto salty;              
            }
            case 2: {
                temp = new Peperoni(temp);
                cout << "Added Peperoni! Type 0 and enter to continue" << endl;
                cin >> var;
                goto salty;
            }    
            case 3: {
                temp = new Onion(temp);
                cout << "Added Onions! Type 0 and enter to continue" << endl;
                cin >> var;
                goto salty;
            }    
            case 4: {
                temp = new Pepper(temp);
                cout << "Added Pepper! Type 0 and enter to continue" << endl;
                cin >> var;
                goto salty;
            }
            case 5: {
                clear();
                cout << "Order was finished!" << endl;
                cout << "Price: R$" << temp->getPrice() << endl;
                cout << "Calories: " << temp->getCal() << " Kcal" << endl;
                goto end;
            }
            default:
                break;
        }
    }
    case 2: {
        temp = new Sweet();
    sweety:
        sweet = sweetMenu();
        switch (sweet) {
            case 1: {
                temp = new Chocolate(temp);
                cout << "Added Chocolate! Type 0 and enter to continue" << endl;
                cin >> var;
                goto sweety;              
            }
            case 2: {
                temp = new CondMilk(temp);
                cout << "Added Condensed Milk! Type 0 and enter to continue" << endl;
                cin >> var;
                goto sweety;
            }    
            case 3: {
                temp = new Strawberry(temp);
                cout << "Added Strawberry! Type 0 and enter to continue" << endl;
                cin >> var;
                goto sweety;
            }    
            case 4: {
                temp = new Sprinkles(temp);
                cout << "Added Sprinkles! Type 0 and enter to continue" << endl;
                cin >> var;
                goto sweety;
            }
            case 5: {
                clear();
                cout << "Order was finished!" << endl;
                cout << "Price: R$" << temp->getPrice() << endl;
                cout << "Calories: " << temp->getCal() << " Kcal" << endl;
                goto end;
            }
            default:
                break;
        }
    }
    default:
        cout << "Not an option! Try again" << endl;
        goto menu;
    }
end:
   return 0;
}