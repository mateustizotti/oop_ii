#ifndef CHEESE_H
#define CHEESE_H
#include <iostream>
#include <string>
#include "../extra.h"

using namespace std;

class Cheese : public Extra{
    private:
        float price = 5;
        long cal = 100;
    public:
        Cheese(Pizza *decoratedPizza) : Extra(decoratedPizza) {}
        int getPrice() { return m_decoratedPizza->getPrice() + price; }
        long getCal() { return m_decoratedPizza->getCal() + cal; }
};

#endif