#ifndef ONION_H
#define ONION_H
#include <iostream>
#include <string>
#include "../extra.h"

using namespace std;

class Onion : public Extra{
    private:
        float price = 1;
        long cal = 5;
    public:
        Onion(Pizza *decoratedPizza) : Extra(decoratedPizza) {}
        int getPrice() { return m_decoratedPizza->getPrice() + price; }
        long getCal() { return m_decoratedPizza->getCal() + cal; }
};

#endif