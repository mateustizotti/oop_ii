#ifndef PEPERONI_H
#define PEPERONI_H
#include <iostream>
#include <string>
#include "../extra.h"

using namespace std;

class Peperoni : public Extra{
    private:
        float price = 6;
        long cal = 80;
    public:
        Peperoni(Pizza *decoratedPizza) : Extra(decoratedPizza) {}
        int getPrice() { return m_decoratedPizza->getPrice() + price; }
        long getCal() { return m_decoratedPizza->getCal() + cal; }
};

#endif