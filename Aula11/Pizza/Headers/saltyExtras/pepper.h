#ifndef PEPPER_H
#define PEPPER_H
#include <iostream>
#include <string>
#include "../extra.h"

using namespace std;

class Pepper : public Extra{
    private:
        float price = 2;
        long cal = 10;
    public:
        Pepper (Pizza *decoratedPizza) : Extra(decoratedPizza) {}
        int getPrice() { return m_decoratedPizza->getPrice() + price; }
        long getCal() { return m_decoratedPizza->getCal() + cal; }
};

#endif