#ifndef PIZZA_H
#define PIZZA_H
#include <iostream>
#include <string>

using namespace std;

class Pizza {
    public:
        virtual int getPrice() = 0;
        virtual long getCal() = 0;
};

#endif