#ifndef EXTRA_H
#define EXTRA_H
#include <iostream>
#include <string>
#include "pizza.h"

using namespace std;

class Extra : public Pizza {
    protected:
        Pizza *m_decoratedPizza;
    public:
        Extra (Pizza *decoratedPizza) : m_decoratedPizza(decoratedPizza) {}
};

#endif