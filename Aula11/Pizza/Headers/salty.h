#ifndef SALTY_H
#define SALTY_H
#include <iostream>
#include <string>
#include "pizza.h"

using namespace std;

class Salty : public Pizza {
    private:
        float price = 20;
        long cal = 120;
    public:
        int getPrice() { return price; }
        long getCal() { return cal; }
};

#endif