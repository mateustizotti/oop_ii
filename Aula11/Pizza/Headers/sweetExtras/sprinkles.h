#ifndef SPRINKLES_H
#define SPRINKLES_H
#include <iostream>
#include <string>
#include "../extra.h"

using namespace std;

class Sprinkles : public Extra{
    private:
        float price = 2;
        long cal = 20;
    public:
        Sprinkles(Pizza *decoratedPizza) : Extra(decoratedPizza) {}
        int getPrice() { return m_decoratedPizza->getPrice() + price; }
        long getCal() { return m_decoratedPizza->getCal() + cal; }
};

#endif