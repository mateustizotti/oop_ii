#ifndef CHOCOLATE_H
#define CHOCOLATE_H
#include <iostream>
#include <string>
#include "../extra.h"

using namespace std;

class Chocolate : public Extra{
    private:
        float price = 9;
        long cal = 150;
    public:
        Chocolate(Pizza *decoratedPizza) : Extra(decoratedPizza) {}
        int getPrice() { return m_decoratedPizza->getPrice() + price; }
        long getCal() { return m_decoratedPizza->getCal() + cal; }
};

#endif