#ifndef CONDMILK_H
#define CONDMILK_H
#include <iostream>
#include <string>
#include "../extra.h"

using namespace std;

class CondMilk : public Extra{
    private:
        float price = 8;
        long cal = 190;
    public:
        CondMilk(Pizza *decoratedPizza) : Extra(decoratedPizza) {}
        int getPrice() { return m_decoratedPizza->getPrice() + price; }
        long getCal() { return m_decoratedPizza->getCal() + cal; }
};

#endif