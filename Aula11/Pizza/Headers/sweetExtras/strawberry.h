#ifndef STRAWBERRY_H
#define STRAWBERRY_H
#include <iostream>
#include <string>
#include "../extra.h"

using namespace std;

class Strawberry : public Extra{
    private:
        float price = 5;
        long cal = 30;
    public:
        Strawberry(Pizza *decoratedPizza) : Extra(decoratedPizza) {}
        int getPrice() { return m_decoratedPizza->getPrice() + price; }
        long getCal() { return m_decoratedPizza->getCal() + cal; }
};

#endif