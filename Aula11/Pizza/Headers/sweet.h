#ifndef SWEET_H
#define SWEET_H
#include <iostream>
#include <string>
#include "pizza.h"

using namespace std;

class Sweet : public Pizza {
    private:
        float price = 20;
        long cal = 150;
    public:
        int getPrice() { return price; }
        long getCal() { return cal; }
};

#endif