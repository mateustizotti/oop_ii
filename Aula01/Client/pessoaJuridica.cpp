#include "Headers/pessoaJuridica.h"

using namespace std;

pessoaJuridica::pessoaJuridica(string name, string add, string phone, string cn, string nf) 
 : Cliente(name, add, phone) {
     cnpj = cn;
     nomeFantasia = nf;
}

void pessoaJuridica::setNomeFantasia(string newNomeFantasia) {
    nomeFantasia = newNomeFantasia;
}

void pessoaJuridica::showData(void) {
    cout << "Nome: " << getName() << endl << "Endereco: " << getEnd() << endl
    << "Telefone: " << getTel() << endl << "CNPJ: " << getCnpj() << endl << "Nome Fantasia: " << getNomeFatnasia() << endl;
}

pessoaJuridica::~pessoaJuridica() {

}