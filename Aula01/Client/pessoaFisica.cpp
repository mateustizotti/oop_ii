#include "Headers/pessoaFisica.h"

using namespace std;

pessoaFisica::pessoaFisica(string name, string add, string phone, string c) : Cliente(name, add, phone) {
    cpf = c;
}

void pessoaFisica::showData(void) {
    cout << "Nome: " << getName() << endl << "Endereco: " << getEnd() << endl
    << "Telefone: " << getTel() << endl << "CPF: " << getCpf() << endl;
}

pessoaFisica::~pessoaFisica() {

}