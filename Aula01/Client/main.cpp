#include <iostream>
#include "Headers/cliente.h"
#include "Headers/pessoaFisica.h"
#include "Headers/pessoaJuridica.h"
#include <locale>

using namespace std;

int main() {

    setlocale( LC_ALL, "Portuguese" );

    Cliente *fisica = new pessoaFisica("User", "Rua Sei La", "99999999999", "123.456.789-00");
    cout << "Pessoa Fisica" << endl;
    fisica->showData();

    cout << endl;

    Cliente *b = new pessoaJuridica("Company", "Av tbm nao sei", "99999999999", "1111.0001/111-99", "Nome Fantasia");
    cout << "Empresa" << endl;
    b->showData();
 
    return 0; 
}