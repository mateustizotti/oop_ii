#ifndef CLIENTE_H
#define CLIENTE_H
#include <iostream>
#include <string>

using namespace std;

class Cliente{
    string nome, end, telefone;
    public:
        Cliente();
        Cliente(string name, string add, string phone);
        string getName() {
            return nome;
        }
        string getEnd() {
            return end;
        }
        string getTel() {
            return telefone;
        }
        void setName(string newName);
        void setEnd(string newEnd);
        void setTel(string newTel);
        ~Cliente();

        virtual void showData() = 0;
};
#endif