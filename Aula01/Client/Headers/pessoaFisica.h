#ifndef PESSOAFISICA_H
#define PESSOAFISICA_H
#include <iostream>
#include <string>
#include "cliente.h"

using namespace std;

class pessoaFisica : public Cliente{
    string cpf;
    public:
        pessoaFisica(string name, string add, string phone, string cx);
        string getCpf() { 
            return cpf;
        }
        void showData(void);
        ~pessoaFisica();
};
#endif