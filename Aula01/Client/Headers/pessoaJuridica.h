#ifndef PESSOAJURIDICA_H
#define PESSOAJURIDICA_H
#include <iostream>
#include <string>
#include "cliente.h"

using namespace std;

class pessoaJuridica : public Cliente{
    public:
    string cnpj, nomeFantasia;
    pessoaJuridica(string name, string add, string phone, string cn, string nf);
    string getCnpj() { 
        return cnpj;
    }
    string getNomeFatnasia() {
        return nomeFantasia;
    }
    void setNomeFantasia(string newNomeFantasia);
    void showData(void);
    ~pessoaJuridica();
};
#endif