#ifndef DATE_H
#define DATE_H
#include <iostream>

using namespace std;

class Date {
    private:
        int day, month, year;
    public:
    Date();
    Date(int d, int m, int y);
    int getDay() { return day; }
    int getMonth() { return month; }
    int getYear() { return year; }

    void setDay(int d);
    void setMonth(int m);
    void setYear(int y);

    void setDate(int d, int m, int y);

    Date operator-(Date newDate);
};
#endif