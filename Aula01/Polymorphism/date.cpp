#include "Headers/date.h"

Date::Date() {
    setDate(0, 0, 0);
};

Date::Date(int d, int m, int y) {
    day = d;
    month = m;
    year = y;
};

void Date::setDay(int d) {
    if (d < 32 && d > 0) {    
        day = d;
    }
};

void Date::setMonth(int m) {
    if (m < 13 && m > 0) {
        month = m;
    }
};

void Date::setYear(int y) {
    year = y;
};

void Date::setDate(int d, int m, int y) {
    setDay(d);
    setMonth(m);
    setYear(y);
};

Date Date::operator-(Date newDate) {
    int new_day, new_month, new_year;
    new_day = day - newDate.day;
    new_month = month - newDate.month;
    new_year = year - newDate.year;

    if (new_day < 0) {
        new_day = new_day*(-1);
    }  
    if (new_month < 0){
        new_month = new_month*(-1);
    }  
    if (new_year < 0) {
        new_year = new_year*(-1);
    }
    
    return Date(new_day, new_month, new_year);
};