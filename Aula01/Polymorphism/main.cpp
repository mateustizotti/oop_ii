#include <iostream>
#include "Headers/date.h"

using namespace std;

int main() {
    Date *d1 = new Date(20, 10, 1999);
    Date *d2 = new Date(5, 7, 1995);
    Date diff = d1->operator-(*d2);
    
    cout << diff.getYear() << " years, " 
    << diff.getMonth() << " months and " 
    << diff.getDay() << " days" << endl;

    return 0;
}